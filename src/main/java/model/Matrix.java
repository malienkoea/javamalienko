package model;

import java.util.ArrayList;
import java.util.List;

public class Matrix {
    private int rawCount = 0;
    private int columnCount = 0;

    List<ArrayList<Person>> matrix;

    public Matrix(int raws, int colums) {
        matrix = new ArrayList<ArrayList<Person>>(raws);
        for (int i = 0; i < raws; i++) {
            matrix.add(new ArrayList<Person>(colums));
        }
        this.rawCount = raws;
        this.columnCount = colums;
    }

    public List<ArrayList<Person>> getMatrix() {
        return matrix;
    }

    public void setMatrix(List<ArrayList<Person>> matrix) {
        this.matrix = matrix;
    }

    public Person getValue(int x, int y) {
        return matrix.get(x).get(y);
    }

    public void setValue(int x, int y, Person value) {
        matrix.get(x).set(y, value);
    }

    public int getRawCount() {
        return rawCount;
    }

    public void setRawCount(int rawCount) {
        this.rawCount = rawCount;
    }

    public int getColumnCount() {
        return columnCount;
    }

    public void setColumnCount(int columnCount) {
        this.columnCount = columnCount;
    }
}


