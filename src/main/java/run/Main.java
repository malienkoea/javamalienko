package run;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import model.Matrix;
import utils.Calculator;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Main {
    private static final Gson GSON = new GsonBuilder().create();

    public static void main(String[] args) throws IOException {
        Matrix matrix = new Matrix(10, 5);
        Calculator calc = new Calculator();
        calc.matrixFill(matrix);
        String json = GSON.toJson(matrix.getMatrix());
        System.out.println(json);
        try (FileWriter writer = new FileWriter("C:/labs/file1.txt")) {

            writer.write(json);

        } catch (IOException e) {
            e.printStackTrace();
        }
        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader("C:/labs/file1.txt"))) {
            String currentLine;
            while ((currentLine = br.readLine()) != null) {
                stringBuilder.append(currentLine);
            }
        }
        System.out.println(stringBuilder.toString().equals(json));
    }
}

