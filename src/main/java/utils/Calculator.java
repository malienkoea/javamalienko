package utils;

import model.Matrix;
import model.Person;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Calculator {

    public List<ArrayList<Person>> matrixFill(Matrix matr) {
        List<ArrayList<Person>> matrPerson = matr.getMatrix();
        Random rnd = new Random(47);
        for (int i = 0; i < matr.getRawCount(); i++) {
            for (int j = 0; j < matr.getColumnCount(); j++) {
                matrPerson.get(i).add(new Person("Liza", 20));
            }
        }
        return matrPerson;
    }

    public void printMatrix(Matrix matr) {
        List<ArrayList<Person>> matrix = matr.getMatrix();
        for (int i = 0; i < matr.getRawCount(); i++) {
            for (int j = 0; j < matr.getColumnCount(); j++) {
                System.out.print(matrix.get(i));
            }
            System.out.println();
        }
    }
}


